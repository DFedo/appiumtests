import driver.AndroidDriverSingleton;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class TestListener implements ITestListener {
    final static Logger logger = Logger.getLogger(TestListener.class);

    @Override
    public void onTestStart(ITestResult result) {
        logger.info("Test has started running:" + result.getMethod().getMethodName()
                + " at: " + result.getStartMillis());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logger.info("Successful test");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        Date currentDate = new Date();
        String screenshotFileName = currentDate.toString().replace(" ", "-").replace(":", "-");
        File file = ((TakesScreenshot) AndroidDriverSingleton.getDriver()).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(file, new File("target/surefire-reports" + screenshotFileName + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("Test was failed");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        logger.info("Test skipped");
    }

    @Override
    public void onFinish(ITestContext context) {
        logger.info("Passed tests: " + context.getPassedTests());
        logger.info("Failed tests: " + context.getFailedTests());
    }
}


