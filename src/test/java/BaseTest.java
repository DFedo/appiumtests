import driver.AndroidDriverSingleton;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import pageObjects.SentLettersPage;

public class BaseTest {

    @AfterSuite
    public void quitDriver() {
        AndroidDriverSingleton.quitDriver();
    }

    public SentLettersPage getSentLettersPage() {
        return new SentLettersPage();
    }
}
