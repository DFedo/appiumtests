import businessObjects.GmailBO;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(TestListener.class)
public class SendMail_Test extends BaseTest {

    private final GmailBO gmailBO = new GmailBO();

    private final String recipient = "accforappiumtests@gmail.com";
    private final String subject = "test email";
    private final String letter = "first successful test";

    @Test(priority = 1)
    public void isLetterSentTest() {
        gmailBO
                .skipStartPage()
                .sendLetter(recipient, subject, letter)
                .openSentLetters();
        Assert.assertTrue(getSentLettersPage().isSentLetterDisplayed(subject));
    }

    @Test(priority = 2)
    public void deleteSentLetter() {
        gmailBO
                .deleteSentLetter(subject);
        Assert.assertTrue(getSentLettersPage().isSentLetterDeleted());
    }
}
