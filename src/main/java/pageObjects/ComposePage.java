package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ComposePage extends BasePage {
    @FindBy(id = "to")
    private WebElement recipientInputField;

    @FindBy(id = "subject")
    private WebElement subjectInputField;

    @FindBy(id = "body")
    private WebElement letterInputField;

    @FindBy(id = "send")
    private WebElement sendButton;

    public ComposePage typeRecipient(String recipient){
        recipientInputField.sendKeys(recipient);
        return this;
    }

    public ComposePage typeSubject(String subject){
        subjectInputField.sendKeys(subject);
        return this;
    }

    public ComposePage typeLetter(String letter){
        letterInputField.sendKeys(letter);
        return this;
    }

    public ComposePage clickSendButton(){
        sendButton.click();
        return this;
    }
}
