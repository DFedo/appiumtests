package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PrimaryPage extends BasePage {
    @FindBy(id = "compose_button")
    private WebElement composeButton;

    @FindBy(xpath = "//android.widget.ImageButton[@content-desc='Open navigation drawer']")
    private WebElement navigationButton;

    @FindBy(xpath = "//android.widget.LinearLayout//android.widget.TextView[@text='Sent']")
    private WebElement sentButton;

    public PrimaryPage clickComposeButton() {
        composeButton.click();
        return this;
    }

    public PrimaryPage clickNavigationButton() {
        navigationButton.click();
        return this;
    }

    public PrimaryPage clickSentButton() {
        sentButton.click();
        return this;
    }
}
