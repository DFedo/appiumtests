package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class StartPage extends BasePage {
    @FindBy(id = "welcome_tour_got_it")
    private WebElement gotItButton;

    @FindBy(id = "action_done")
    private WebElement takeMeToGmailButton;

    @FindBy(id = "manual_sync")
    private WebElement manualSyncButton;

    @FindBy(id = "empty_text")
    private WebElement welcomeMessage;

    public StartPage clickGotItButton() {
        gotItButton.click();
        return this;
    }

    public StartPage clickTakeMeToGmailButton() {
        takeMeToGmailButton.click();
        return this;
    }

    public WebElement welcomeMessage() {
        return welcomeMessage;
    }

    public StartPage clickManualSyncButton() {
        manualSyncButton.click();
        return this;
    }
}
