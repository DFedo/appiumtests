package pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EmailPage extends BasePage {
    @FindBy(id = "delete")
    private WebElement deleteEmailButton;

    public EmailPage clickDeleteEmailButton() {
        deleteEmailButton.click();
        return this;
    }
}
