package pageObjects;

import driver.AndroidDriverSingleton;
import org.openqa.selenium.By;

public class SentLettersPage extends BasePage{
    public boolean isSentLetterDisplayed(String subject) {
        String letterSubject = String.format("//android.view.View[contains(@content-desc, '%s')]", subject);
        return AndroidDriverSingleton.getDriver().findElement(By.xpath(letterSubject)).isDisplayed();
    }

    public  SentLettersPage clickOnSentLetter(String subject) {
        String letterSubject = String.format("//android.view.View[contains(@content-desc, '%s')]", subject);
        AndroidDriverSingleton.getDriver().findElement(By.xpath(letterSubject)).click();
        return this;
    }

    public boolean  isSentLetterDeleted(){
        return AndroidDriverSingleton.getDriver().findElement(By.id("empty_text")).isDisplayed();
    }
}
