package businessObjects;

import com.sun.media.jfxmedia.logging.Logger;
import org.testng.Reporter;
import pageObjects.*;

public class GmailBO {
    private final StartPage startPage;
    private final ComposePage composePage;
    private final PrimaryPage primaryPage;
    private final SentLettersPage sentLettersPage;
    private final EmailPage emailPage;

    public GmailBO() {
        startPage = new StartPage();
        composePage = new ComposePage();
        primaryPage = new PrimaryPage();
        sentLettersPage = new SentLettersPage();
        emailPage = new EmailPage();
    }

    public GmailBO skipStartPage() {
        startPage
                .clickGotItButton()
                .clickTakeMeToGmailButton()
                .waitVisibilityOfElement(15000, startPage.welcomeMessage());
        Reporter.log("Start page is skipped");
        return this;
    }

    public GmailBO sendLetter(String recipient, String subject, String letter) {
        primaryPage.clickComposeButton();
        composePage
                .typeRecipient(recipient)
                .typeSubject(subject)
                .typeLetter(letter)
                .clickSendButton();
        Reporter.log("Letter is sent");
        return this;
    }

    public GmailBO openSentLetters() {
        primaryPage
                .clickNavigationButton()
                .clickSentButton();
        return this;
    }

    public GmailBO deleteSentLetter(String subject) {
        sentLettersPage.clickOnSentLetter(subject);
        emailPage.clickDeleteEmailButton();
        Reporter.log("Letter is deleted");
        return this;
    }
}

